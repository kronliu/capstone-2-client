if (localStorage.getItem("token") !== null) {
  if (localStorage.getItem("isAdmin") === "true") {
    document.querySelector("#profile").innerHTML = "";
  }
  document.querySelector("#register").innerHTML = "";
  document.querySelector("#login").innerHTML = "";
} else {
  document.querySelector("#logout").innerHTML = "";
  document.querySelector("#profile").innerHTML = "";
}

if (localStorage.getItem("isAdmin") == "true") {
  document.querySelector("#profile").innerHTML = "";
}

let formSubmit = document.querySelector("#createCourse");

formSubmit.addEventListener("submit", (e) => {
  e.preventDefault();

  let courseName = document.querySelector("#courseName").value;
  let description = document.querySelector("#courseDescription").value;
  let price = document.querySelector("#coursePrice").value;

  let token = localStorage.getItem("token");

  fetch("https://intense-waters-48513.herokuapp.com/api/courses", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      name: courseName,
      description: description,
      price: price,
    }),
  })
    .then((res) => {
      return res.json();
    })
    .then((data) => {
      if (data) {
        window.location.replace("./courses.html");
      } else {
        alert("Course not added");
      }
    });
});
