if (localStorage.getItem("token") !== null) {
  if (localStorage.getItem("isAdmin") === "true") {
    document.querySelector("#profile").innerHTML = "";
  }
  document.querySelector("#register").innerHTML = "";
  document.querySelector("#login").innerHTML = "";
} else {
  document.querySelector("#logout").innerHTML = "";
  document.querySelector("#profile").innerHTML = "";
}

let loginForm = document.querySelector("#logInUser");

loginForm.addEventListener("submit", (e) => {
  e.preventDefault();

  let email = document.querySelector("#userEmail").value;
  let password = document.querySelector("#password").value;

  if (email == "" || password == "") {
    alert("Invalid Input");
  } else {
    fetch("https://intense-waters-48513.herokuapp.com/api/users/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        console.log(data);
        if (data.access !== null && data !== false) {
          localStorage.setItem("token", data.access);
          fetch(
            "https://intense-waters-48513.herokuapp.com/api/users/details",
            {
              headers: {
                Authorization: `Bearer ${data.access}`,
              },
            }
          )
            .then((res) => {
              return res.json();
            })
            .then((data) => {
              console.log(data);
              console.log(data.isAdmin);
              localStorage.setItem("isAdmin", data.isAdmin);
              window.location.replace("./../index.html");
            });
        } else {
          alert(
            "Unable to login. Password might be incorrect or user does not exists."
          );
        }
      });
  }
});
