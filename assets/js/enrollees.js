if (localStorage.getItem("token") !== null) {
  if (localStorage.getItem("isAdmin") === "true") {
    document.querySelector("#profile").innerHTML = "";
  }
  document.querySelector("#register").innerHTML = "";
  document.querySelector("#login").innerHTML = "";
} else {
  document.querySelector("#logout").innerHTML = "";
  document.querySelector("#profile").innerHTML = "";
}

let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");

let container = document.querySelector("#enrolleesContainer");

fetch(`https://intense-waters-48513.herokuapp.com/api/courses/${courseId}`)
  .then((res) => {
    return res.json();
  })
  .then((data) => {
    courseName.innerHTML = data.name;
    courseDesc.innerHTML = data.description;

    data.enrollees.map((element) => {
      let enrolleesId = element.userId;
      let uniqueSet = [...new Set(enrolleesId)];

      fetch(
        `https://intense-waters-48513.herokuapp.com/api/users/enrollees/${element.userId}`
      )
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          if (data.firstname !== undefined && data.firstname !== undefined) {
            return (container.innerHTML += `            
            <div class="col-md-6 my-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">${data.firstname} ${data.lastname}</h5>
                    </div>
                </div>
            </div>
            
            `);
          }
        });
    });
  });
