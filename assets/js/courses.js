if (localStorage.getItem("token") !== null) {
  if (localStorage.getItem("isAdmin") === "true") {
    document.querySelector("#profile").innerHTML = "";
  }
  document.querySelector("#register").innerHTML = "";
  document.querySelector("#login").innerHTML = "";
} else {
  document.querySelector("#logout").innerHTML = "";
  document.querySelector("#profile").innerHTML = "";
}

let coursesContainer = document.querySelector("#coursesContainer");
let adminButton = document.querySelector("#adminButton");
let courseData;
let archiveToggle;
let cardFooter;

if (localStorage.getItem("isAdmin") === "true") {
  fetch("https://intense-waters-48513.herokuapp.com/api/courses/")
    .then((res) => res.json())
    .then((data) => {
      if (data.length < 1) {
        courseData = "No courses available";
      } else {
        courseData = data
          .map((course) => {
            if (course.isActive == true) {
              cardHeader = `<span class="card-header-green">Active</span>`;
              archiveToggle = `    
              <a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="my-button archive-button text-white text-center my-1" id="archiveButton">
              Archive Course
              </a>
              `;
            } else {
              cardHeader = `<span class="card-header-red">Archived</span>`;
              archiveToggle = `                
              <a href="./enableCourse.html?courseId=${course._id}" value=${course._id} class="my-button enable-button text-white text-center my-1" id="enableButton">
              Enable Course
              </a>`;
            }
            cardFooter = `
            ${archiveToggle}
            <a type="button" id="editCourseButton" 
            class="my-button edit-course-button text-center text-white my-1 ml-2" data-toggle="modal" data-target="#exampleModal" data-id=${course._id} onclick="myFunction(this)">
              Edit Course
            </a>
            <a href="./enrollees.html?courseId=${course._id}" value=${course._id} class="my-button edit-course-button text-white text-center ml-2 my-1" id="viewEnrolleesButton">
              View Enrollees
            </a>

          `;
            return `
              <div class="col-12 col-md-6 my-3">

                  <div class="box shadow course-box">
                    <div class="contentBox">
                      <div>
                        <div class="row">
                          <h4 class="col-4 col-md-9 course-name text-dark ">${course.name}</h4>
                          <span class="ml-auto mr-3">${cardHeader}</span>
                        </div>
                        <div class="row">
                        <p class="course-description text-dark ml-3 mt-2">
                        <span class="text-muted"> Description: </span>${course.description}
                        </p>
                        </div>
                        <div class="row">
                        <p class="course-price text-dark ml-3">
                          <span class="text-muted">Price: </span>${course.price} PHP
                        </p>
                        </div>
                        <div class="text-right">${cardFooter}</div>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
      `;
          })
          .join("");
      }

      coursesContainer.innerHTML = courseData;
    });
} else {
  adminButton.innerHTML = null;
  fetch("https://intense-waters-48513.herokuapp.com/api/courses/active")
    .then((res) => res.json())
    .then((data) => {
      let courseData;
      if (data.length < 1) {
        courseData = "No courses available";
      } else {
        courseData = data
          .map((course) => {
            return `
            <div class="col-12 col-md-6 my-3">
              <a class="courseLink" href="./course.html?courseId=${course._id}" target="_blank">
                <div class="box shadow course-box">
                  <div class="contentBox">
                    <div>
                      <h4 class="course-name text-dark">${course.name}</h4>
                      <p class="course-description text-dark"><span class="text-muted"> Description: </span>${course.description}</p>
                      <p class="course-price text-dark"><span class="text-muted">Price: </span>${course.price} PHP</p>
                      <div class="text-right"><a href="./course.html?courseId=${course._id}" class="my-button course-button text-white">
                      Go to Course
                      </a>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>
      `;
          })
          .join("");
      }
      coursesContainer.innerHTML = courseData;
    });
}

// <a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="my-button text-center my-1" id="editCourse">
// Edit Course
// </a>

let formSubmit = document.querySelector("#editCourse");

formSubmit.addEventListener("submit", (e) => {
  e.preventDefault();

  let courseName = document.querySelector("#inputCourseName").value;
  let description = document.querySelector("#inputCourseDesc").value;
  let price = document.querySelector("#inputCoursePrice").value;

  if (courseName !== "" && description !== "" && price > 0) {
    fetch("https://intense-waters-48513.herokuapp.com/api/courses", {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        courseId: courseId,
        name: courseName,
        description: description,
        price: price,
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        if (data) {
          alert("Edited Successfully");
        } else {
          alert("Edit Failed");
        }
      });
  } else {
    alert("Invalid Input");
  }
  // let token = localStorage.getItem("token");
});

let courseId;
function myFunction(e) {
  courseId = e.getAttribute("data-id");
}

function closeModal() {
  window.location.replace("./courses.html");
}
