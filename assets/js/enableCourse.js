let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");
let message = document.querySelector("#message");
let token = localStorage.getItem("token");

fetch(
  `https://intense-waters-48513.herokuapp.com/api/courses/enable/${courseId}`,
  {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }
)
  .then((res) => {
    return res.json();
  })
  .then((data) => {
    if (data) {
      message.innerHTML = "Course Enabled";
    } else {
      message.innerHTML = "Failed to Enable Course";
    }
  });
