if (localStorage.getItem("token") !== null) {
  if (localStorage.getItem("isAdmin") === "true") {
    document.querySelector("#profile").innerHTML = "";
  }
  document.querySelector("#register").innerHTML = "";
  document.querySelector("#login").innerHTML = "";
} else {
  document.querySelector("#logout").innerHTML = "";
  document.querySelector("#profile").innerHTML = "";
}

let token = localStorage.getItem("token");

let name = document.querySelector("#name");
let email = document.querySelector("#email");
let mobileNo = document.querySelector("#mobileNo");

let enrolledCoursesContainer = document.querySelector(
  "#enrolledCoursesContainer"
);

fetch("https://intense-waters-48513.herokuapp.com/api/users/details", {
  headers: {
    Authorization: `Bearer ${token}`,
  },
})
  .then((res) => {
    return res.json();
  })
  .then((data) => {
    console.log(data);

    name.innerText = `${data.firstname} ${data.lastname}`;
    email.innerText = data.email;
    mobileNo.innerText = data.mobileNo;

    if (data.enrollments.length == 0) {
      enrolledCoursesContainer.innerHTML =
        "<p class='text-muted ml-3'>Not Enrolled in Any Course</p>";
    }

    for (var i = 0; i < data.enrollments.length - 1; i++) {
      fetch(
        `https://intense-waters-48513.herokuapp.com/api/courses/${data.enrollments[i].courseId}`,
        {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
          },
        }
      )
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          enrolledCoursesContainer.innerHTML += `
            <div class="col-12 col-md-6 my-3">
              <a class="courseLink" href="./course.html?courseId=${data._id}" target="_blank">
                <div class="box shadow course-box">
                  <div class="contentBox">
                    <div>
                      <h4 class="course-name text-dark">${data.name}</h4>
                      <p class="course-description text-dark">${data.description}</p>
                      <div class="text-right"><a href="./course.html?courseId=${data._id}" class="my-button course-button text-white">
                      Go to Course
                      </a>
                      </div>
                    </div>
                  </div>
                </div>
              </a>
            </div>
`;
        });
    }
  });
