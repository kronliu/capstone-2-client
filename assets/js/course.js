if (localStorage.getItem("token") !== null) {
  if (localStorage.getItem("isAdmin") === "true") {
    document.querySelector("#profile").innerHTML = "";
  }
  document.querySelector("#register").innerHTML = "";
  document.querySelector("#login").innerHTML = "";
} else {
  document.querySelector("#logout").innerHTML = "";
  document.querySelector("#profile").innerHTML = "";
}

let adminUser = localStorage.getItem("isAdmin");

let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");

let token = localStorage.getItem("token");
let userId = localStorage.getItem("id");

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");

fetch(`https://intense-waters-48513.herokuapp.com/api/courses/${courseId}`)
  .then((res) => {
    return res.json();
  })
  .then((data) => {
    courseName.innerHTML = data.name;
    courseDesc.innerHTML = ` Description: ${data.description}`;
    coursePrice.innerHTML = ` Price: ${data.price} PHP`;
    if (adminUser == null || adminUser == undefined || adminUser == "") {
      enrollContainer.innerHTML = `
    <a href="./courses.html" class="btn btn-block btn-primary mt-3">Go Back to Courses</a>
  `;
    } else {
      enrollContainer.innerHTML = `
      <div class="row justify-content-center align-items-center"><button id="enrollButton" class="my-button course-button px-5">Enroll</button></div>
      <div class="row justify-content-center align-items-center"><a href="./courses.html" class="my-button course-button mt-3 px-3">Back to Courses</a>
      </div>

    `;

      document.querySelector("#enrollButton").addEventListener("click", () => {
        fetch("https://intense-waters-48513.herokuapp.com/api/users/enroll", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            userId: userId,
            courseId: courseId,
          }),
        })
          .then((res) => {
            return res.json();
          })
          .then((data) => {
            if (data) {
              alert("You have enrolled successfully");
              window.location.replace("./courses.html");
            } else {
              alert("Enrollment failed");
            }
          });
      });
    }
  });
