let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");
let message = document.querySelector("#message");
let token = localStorage.getItem("token");

fetch(
  `https://intense-waters-48513.herokuapp.com/api/courses/archive/${courseId}`,
  {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }
)
  .then((res) => {
    return res.json();
  })
  .then((data) => {
    if (data) {
      message.innerHTML = "Course Archived";
    } else {
      message.innerHTML = "Unable to Archive Course";
    }
  });
